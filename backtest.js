var EventEmitter = require("events").EventEmitter;
var https = require("https");
var http = require("http");
var util = require("util");

/**
 * An EventEmitter to get a backest report
 * @constructor
 */
function Backtest(backtestid) {

    EventEmitter.call(this);

    backtestEmitter = this;

    var options = {
        hostname: 'localhost',
        port: 8080,
        path: '/candles/gdax/BTCUSD',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    var req = http.request(options, function (response) {
        console.log('Status: ' + response.statusCode);
        console.log('Headers: ' + JSON.stringify(response.headers));

        var body = "";
        if (response.statusCode !== 200) {
            request.abort();
            //Status Code Error
            backtestEmitter.emit("error", new Error("There was an error getting the backtest ID " + backtestid + ". (" + http.STATUS_CODES[response.statusCode] + ")"));
        }

        //Read the data
        response.on('data', function (chunk) {
            body += chunk;
            backtestEmitter.emit("data", chunk);
        });

        response.on('end', function () {
            if (response.statusCode === 200) {
                try {
                    //Parse the data
                    var backtest = JSON.parse(body);
                    backtestEmitter.emit("end", backtest);
                } catch (error) {
                    backtestEmitter.emit("error", error);
                }
            }
        }).on("error", function (error) {
            backtestEmitter.emit("error", error);
        });
    });
    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
        backtestEmitter.emit("error", error);
    });

    // write data to request body
    req.write('{"StartTime": "2018-05-21T09:01:00.000Z", "EndTime": "2018-05-23T13:09:00.000Z","IntervalCandle": 1,"NumMaxBuckets": 300000000}');
    req.end();

    //Connect to the API URL (https://teamtreehouse.com/backtestid.json)
    // var request = https.get("https://teamtreehouse.com/" + backtestid + ".json", function (response) {
    //     var body = "";

    //     if (response.statusCode !== 200) {
    //         request.abort();
    //         //Status Code Error
    //         backtestEmitter.emit("error", new Error("There was an error getting the backtest ID " + backtestid + ". (" + http.STATUS_CODES[response.statusCode] + ")"));
    //     }

    //     //Read the data
    //     response.on('data', function (chunk) {
    //         body += chunk;
    //         backtestEmitter.emit("data", chunk);
    //     });

    //     response.on('end', function () {
    //         if (response.statusCode === 200) {
    //             try {
    //                 //Parse the data
    //                 var backtest = JSON.parse(body);
    //                 backtestEmitter.emit("end", backtest);
    //             } catch (error) {
    //                 backtestEmitter.emit("error", error);
    //             }
    //         }
    //     }).on("error", function (error) {
    //         backtestEmitter.emit("error", error);
    //     });
    // });

}

util.inherits(Backtest, EventEmitter);

module.exports = Backtest;