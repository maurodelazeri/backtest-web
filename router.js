const Backtest = require("./backtest.js");
var render = require("./render");
var querystring = require("querystring");
var commonHeader = {
  'Content-Type': 'html'
};

function home(req, res) {
  if (req.url === "/") {
    if (req.method.toLowerCase() === "get") {
      res.writeHead(200, commonHeader);
      render.view("header", {}, res);
      render.view("search", {}, res);
      render.view("footer", {}, res);
      res.end();
    } else {
      req.on("data", function (postBody) {
        var query = querystring.parse(postBody.toString());
        res.writeHead(303, {
          "location": "/" + query.backtestid
        });
        res.end();
      })
    }
  }
}

function backtest(req, res) {
  var backtestid = req.url.replace("/", "");
  if (backtestid.length > 0) {
    var backtestReport = new Backtest(backtestid)
    backtestReport.on('end', function (data) {
      var values = {
        // avatarUrl: data.gravatar_url,
        // backtestid: data.backtest_name,
        // badges: data.badges.length,
        // javascript: data.points.JavaScript
      }
      res.writeHead(200, commonHeader);
      render.view("header", {}, res);
      render.view("backtest", values, res);
      render.view("footer", {}, res);
      res.end();
    });

    backtestReport.on('error', function (err) {
      render.view("header", {}, res);
      render.view("error", {
        errorMessage: err.message
      }, res)
      render.view("search", {}, res);
      render.view("footer", {}, res);
      res.end();
    });
  }
}

module.exports.home = home;
module.exports.backtest = backtest;